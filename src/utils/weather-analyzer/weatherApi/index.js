import axios from 'axios'
import config from '../../../config.json'

class WeatherApi {

    constructor() {
        this.axios = axios.create({
            baseURL: 'https://api.openweathermap.org/data/2.5/'
        })
        axios.defaults.params = {
            appid: config.weatherApiKey,
            mode: "json"
        }
    }

    fetch5dayForecast({city, country}) {
        return this.axios.get('forecast', {
            params: {
                q: [city, country].join()
            }
        })
    }
}

export default new WeatherApi();