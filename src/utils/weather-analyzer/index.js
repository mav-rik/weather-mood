import WeatherApi from "./weatherApi"
import moodConfig from "./moodConfig.json"

const cacheAliveTime = 10 * 60 * 1000; // 10 minutes

let cache = {}

function writeCache(key, time, value) {
    cache[key] = {time, value}
    return cache[key]
}

export default class WeatherAnalyzer {

    constructor(data) {
        this.data = data;
    }

    _fetchData() {
        return WeatherApi.fetch5dayForecast(this.data)
    }

    getWeatherMood() {
        return new Promise((resolve, reject) => {
            let now = Date.now();
            let key = [this.data.city, this.data.country].join();
            if (cache[key] && (now - cache[key].time) < cacheAliveTime) {
                resolve(cache[key].value)
            } else {
                let value = 0;
                this._fetchData()
                    .then((response) => {
                        value = this._analyzeData(response.data)
                        writeCache(key, now, value)
                        resolve(value)
                    })
                    .catch(reject)
            }
        });
    }

    _analyzeData(data) {
        let result = 0;
        let maxResult = 0;
        let elementMood = 0;

        for (let i = 0; i < data.list.length; i++) {
            const item = data.list[i];
            for (let group in moodConfig) {
                for (let criteria in moodConfig[group]) {
                    if (item[group] && item[group].hasOwnProperty(criteria)) {
                        elementMood = calcElementMood(item[group][criteria], moodConfig[group][criteria])
                    } else {
                        elementMood = moodConfig[group][criteria].weight
                    }
                    result += elementMood
                    maxResult += moodConfig[group][criteria].weight
                }
            }
        }

        function calcElementMood(value, criteriaData) {
            let delta, maxDelta;
            if (criteriaData.hasOwnProperty("range")) {
                delta = Math.abs(value - criteriaData.positive);
                maxDelta = criteriaData.range;
            } else {
                delta = value - criteriaData.positive;
                maxDelta = criteriaData.negative - criteriaData.positive;
            }
            let result = delta / maxDelta;
            if (result > 1) result = 1;
            if (result < 0) result = 0;
            return (1 - result) * criteriaData.weight;
        }

        return Math.round(result / maxResult * 100)
    }


}
